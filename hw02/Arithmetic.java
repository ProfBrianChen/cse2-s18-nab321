//Natasha Baker, Homework 2, CSE 2, due Feb 6 2018
public class Arithmetic {
  public static void main(String [] args) {
    int numPants = 3;//number of pairs of pants
    double pantsPrice = 34.98; //cost per pair of pants
    int numShirts = 2; //number of sweatshirts
    double shirtPrice = 24.99;//cost per shirt
    int numBelts = 1; //number of belts
    double beltPrice = 33.99; //cost per belt
    double paSalesTax = 0.06; //the tax rate
    double totalCostOfPantsWithTax; //total cost of pants with tax
    double totalCostOfShirtsWithTax; //total cost of shirts with tax
    double totalCostOfBeltsWithTax; //total cost of belts with tax
    double totalCostOfPants; //total cost of pants
    double totalCostOfShirts; //total cost of shirts
    double totalCostOfBelts; //total cost of belts
    double pantsTax; //sales tax on pants
    double shirtsTax; //sales tax on shirts
    double beltsTax; //sales tax on belts
    double totalTax; //total sales tax on all items
    double totalCostNoTax; //total cost excluding tax
    double totalCostTax; //total cost including tax
    totalCostOfPants = numPants*pantsPrice; //cost of pants before tax
    pantsTax = totalCostOfPants*paSalesTax; //tax on pants 
    double fixedPantsTax1; //getting rid of extra decimals
    double fixedPantsTax2;
    fixedPantsTax1 = pantsTax*100;
    fixedPantsTax2 = (int) fixedPantsTax1/100;
    totalCostOfPantsWithTax = fixedPantsTax2+totalCostOfPants; //total cost of pants with tax
    totalCostOfShirts = numShirts*shirtPrice; //cost of shirts before tax 
    shirtsTax = totalCostOfShirts*paSalesTax; //tax on shirts 
    double fixedShirtsTax1; //getting rid of extra decimals
    double fixedShirtsTax2;
    fixedShirtsTax1 = shirtsTax*100;
    fixedShirtsTax2 = (int) fixedShirtsTax1/100;
    totalCostOfShirtsWithTax = fixedShirtsTax2+totalCostOfShirts; //total cost of shirts with tax
    totalCostOfBelts = numBelts*beltPrice; //cost of belts before tax 
    beltsTax = totalCostOfBelts*paSalesTax; //tax on belts 
    double fixedBeltsTax1; //getting rid of extra decimals
    double fixedBeltsTax2;
    fixedBeltsTax1 = beltsTax*100;
    fixedBeltsTax2 = (int) fixedBeltsTax1/100;
    totalCostOfBeltsWithTax = totalCostOfBelts+fixedBeltsTax2; //total cost of belts with tax
    totalCostNoTax = totalCostOfPants+totalCostOfShirts+totalCostOfBelts; //total cost before tax
    totalTax = fixedPantsTax2+fixedShirtsTax2+fixedBeltsTax2; //total sales tax
    totalCostTax = totalCostOfPantsWithTax+totalCostOfShirtsWithTax+totalCostOfBeltsWithTax; //total cost of entire purcahse
    System.out.println("Here is your total purchase breakdown:"); //print introduction
    System.out.println(""); //spacing
    System.out.println("Cost of pants before tax: $"+totalCostOfPants); //print pants cost
    System.out.println("Sales tax on pants: $"+fixedPantsTax2); //print tax on pants
    System.out.println("Total cost of pants including tax: $"+totalCostOfPantsWithTax); //print total cost of pants
    System.out.println(""); //spacing 
    System.out.println("Cost of shirts before tax: $"+totalCostOfShirts); //print shirts cost
    System.out.println("Sales tax on shirts: $"+fixedShirtsTax2); //print tax on shirts
    System.out.println("Total cost of shirts including tax: $"+totalCostOfShirtsWithTax); //print total cost of shirts 
    System.out.println(""); //spacing 
    System.out.println("Cost of belts before tax: $"+totalCostOfBelts); //print belts cost
    System.out.println("Sales tax on belts: $"+fixedBeltsTax2); //print tax on belts
    System.out.println("Total cost of belts including tax: $"+totalCostOfBeltsWithTax); //print total cost of belts 
    System.out.println(""); //spacing
    System.out.println("Total cost before tax: $"+totalCostNoTax); //print total cost without tax
    System.out.println("Total sales tax: $"+totalTax); //print total sales tax
    System.out.println("Total cost including tax: $"+totalCostTax); //print total cost
  }
}