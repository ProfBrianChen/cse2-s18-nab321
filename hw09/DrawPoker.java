//Natasha Baker
//CSE2 hw09: Draw poker cards
//due April 17 2018

public class DrawPoker { //creating class
  
  public static boolean pair (int[] list) { //creating method to find pairs
    boolean pair1 = false; //sets boolean equal to false
    for ( int i = 0; i < list.length-1; i++ ) {
      if ( list[i]/13 == list[i+1]/13 ) {
        pair1 = true;
      }
    }
    return pair1; //returns boolean
  }
  
  public static boolean threeKind (int[] list) { //creating method to find three of a kind
    boolean threeKind1 = false; //sets boolean equal to false
    for ( int i = 0; i < list.length-2; i++ ) {
      if ( list[i] == list[i+1] && list[i] == list[i+2] ){
        threeKind1 = true;
      }
    }
    return threeKind1; //returns the boolean
  }
  
  public static boolean flush (int[] list) { //creating method to find a flush
    boolean flush1 = false; //sets boolean equal to false
    for ( int i = 0; i < list.length-1; i++ ) {
      if ( list[i]%13 == list[i+1]%13 ) {
        flush1 = true;
      }
      else {
        flush1 = false;
        break;
      }
    }
    return flush1;
  }
  
  public static boolean fullHouse (int[] list) { //creating method to find a full house
    boolean fullHouse1 = false;
    if ( list[0] == list[1] && list[2] == list[3] && list[2] == list[4] ) {
      fullHouse1 = true;
    }
    else if ( list[1] == list[2] && list[0] == list[3] && list[0] == list[4] ) {
      fullHouse1 = true;
    }
    else if ( list[2] == list[3] && list[0] == list[1] && list[0] == list[4] ) {
      fullHouse1 = true;
    }
    else if ( list[3] == list[4] && list[1] == list[2] && list[1] == list[0] ) {
      fullHouse1 = true;
    }
    return fullHouse1;
  }
  
  public static void main(String[] args) { //creating main method
    int numCards = 52; //initiating number of cards - 1 (0 counts as a card)
    int[] card = new int[numCards]; //initiating array for cards
    for ( int i = 0; i < numCards; i++ ) { //creating loop to assign each number to an array member
      card[i] = i; //card[0] = 0, card[1] = 1, etc.
    }
    
    for ( int j = 0; j < card.length; j++ ) { //creating loop to shuffle the cards
      int first = (int) ( card.length*Math.random() ); //picks a random member to swap with
      int second = card[first];
      card[first] = card[j];
      card[j] = second;
    }
    
    int hand = 5; //number of cards in a hand
    int[] player1 = new int[hand]; //initiating array for player1
    int[] player2 = new int[hand]; //initiating array for player2
    int m = 0; //player1 array number
    int p = 0; //player2 array number
    for ( int n = 0; n < hand*2; n++ ) { //loop to assign cards to each player
      if ( n%2 == 0 ) { //of the first 0-9 cards, card 0,2,4,6,8 will go to player1
        player1[m] = card[n]; //sets the value of that card to the array for player1
        m++; //increments the array member count
      }
      else if ( n%2 == 1 ) { //card 1,3,5,7,9 will go to player2
        player2[p] = card[n]; //sets the value of that card to the array for player2 
        p++; //increments the array member count
      }
    }
    
    System.out.println("PLAYER 1'S CARDS:");
    for ( int k = 0; k < hand; k++ ) { //loop to print player1's cards     
      //determines the number of the card
      if ( player1[k]%13 == 0 ) {
        System.out.print("ACE of ");
      }
      else if ( player1[k]%13 == 1) {
        System.out.print("2 of ");
      }
      else if ( player1[k]%13 == 2) {
        System.out.print("3 of ");
      }
      else if ( player1[k]%13 == 3) {
        System.out.print("4 of ");
      }
      else if ( player1[k]%13 == 4) {
        System.out.print("5 of ");
      }
      else if ( player1[k]%13 == 5) {
        System.out.print("6 of ");
      }
      else if ( player1[k]%13 == 6) {
        System.out.print("7 of ");
      }
      else if ( player1[k]%13 == 7) {
        System.out.print("8 of ");
      }
      else if ( player1[k]%13 == 8) {
        System.out.print("9 of ");
      }
      else if ( player1[k]%13 == 9) {
        System.out.print("10 of ");
      }
      else if ( player1[k]%13 == 10) {
        System.out.print("JACK of ");
      }
      else if ( player1[k]%13 == 11) {
        System.out.print("QUEEN of ");
      }
      else if ( player1[k]%13 == 12) {
        System.out.print("KING of ");
      }
      //determines the suit of the card
      if ( player1[k]/13 < 1 ) {
        System.out.print("DIAMONDS ");
      }
      else if ( player1[k]/13 < 2 ) {
        System.out.print("CLUBS ");
      }
      else if ( player1[k]/13 < 3 ) {
        System.out.print("HEARTS ");
      }
      else {
        System.out.print("SPADES ");
      }
      System.out.println();
    }
    
    
    System.out.println("PLAYER 2'S CARDS:");
    for ( int k = 0; k < hand; k++ ) { //loop to print player2's cards
      //determines the number of the card
      if ( player2[k]%13 == 0 ) {
        System.out.print("ACE of ");
      }
      else if ( player2[k]%13 == 1) {
        System.out.print("2 of ");
      }
      else if ( player2[k]%13 == 2) {
        System.out.print("3 of ");
      }
      else if ( player2[k]%13 == 3) {
        System.out.print("4 of ");
      }
      else if ( player2[k]%13 == 4) {
        System.out.print("5 of ");
      }
      else if ( player2[k]%13 == 5) {
        System.out.print("6 of ");
      }
      else if ( player2[k]%13 == 6) {
        System.out.print("7 of ");
      }
      else if ( player2[k]%13 == 7) {
        System.out.print("8 of ");
      }
      else if ( player2[k]%13 == 8) {
        System.out.print("9 of ");
      }
      else if ( player2[k]%13 == 9) {
        System.out.print("10 of ");
      }
      else if ( player2[k]%13 == 10) {
        System.out.print("JACK of ");
      }
      else if ( player2[k]%13 == 11) {
        System.out.print("QUEEN of ");
      }
      else if ( player2[k]%13 == 12) {
        System.out.print("KING of ");
      }
      //determines the suit of the card
      if ( player2[k]/13 < 1 ) {
        System.out.print("DIAMONDS ");
      }
      else if ( player2[k]/13 < 2 ) {
        System.out.print("CLUBS ");
      }
      else if ( player2[k]/13 < 3 ) {
        System.out.print("HEARTS ");
      }
      else {
        System.out.print("SPADES ");
      }
      System.out.println();
    }
    
    int player1Score = 0; //score to determine the winner
    int player2Score = 0;
    
    pair(player1); //checks if player1's hand has a pair
    boolean pair1;
    if ( pair1 = true ) {
      player1Score += 10;
    }
    pair1 = false; //resets boolean
    pair(player2); //checks if player2's hand has a pair
    if ( pair1 = true ) {
      player2Score += 10;
    }
    
    threeKind(player1); //checks if player1's hand has a three of a kind
    boolean threeKind1;
    if ( threeKind1 = true ) {
      player1Score += 30; //this way even if the other player has two pairs player1 still wins
    }
    threeKind1 = false; //resets boolean
    threeKind(player2); //checks if player2's hand has a three of a kind
    if ( threeKind1 = true ) {
      player2Score += 30;
    }
    
    flush(player1); //checks if player1's hand has a flush
    boolean flush1;
    if ( flush1 = true ) {
      player1Score += 50;
    }
    flush1 = false; //resets boolean
    flush(player2); //checks if player2's hand has a flush
    if ( flush1 = true ) {
      player2Score += 50; //adds to player2's score
    }
    
    fullHouse(player1); //calling fullHouse method for player1
    boolean fullHouse1;
    if ( fullHouse1 = true ) {
      player1Score += 200;
    }
    fullHouse1 = false; //resets boolean
    fullHouse(player2);
    if ( fullHouse1 = true ) {
      player2Score += 200;
    }
    
    if ( player1Score > player2Score ) {
      System.out.println("PLAYER 1 WINS.");
    }
    else if ( player1Score < player2Score ) {
      System.out.println("PLAYER 2 WINS.");
    }
    else if ( player1Score == player2Score ) {
      System.out.println("TIED.");
    }
    
    
  } //end of main method
} //end of class