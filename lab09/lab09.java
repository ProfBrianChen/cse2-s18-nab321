//Natasha Baker
//CSE2: lab09 
//April 13 2018
//passing arrays as method arguments, how the arrays are affected by java's call by value nature

public class lab09 { //creating class
  
  public static int[] copy( int[] list ) { //creating copy method
    int[] list2 = new int[list.length];
    for ( int i = 0; i < list.length; i++ ) {
      list2[i] = list[i];
    }
    return list2; //returns the array
  }
  
  public static void inverter( int[] list) {
    int[] list2 = new int[list.length];
    for (int i = 0; i< list.length; i++){
      list2[i]=list[i];
    }
    for ( int i = 0; i < list.length; i++ ) {
      list[i] = list2[list.length-1-i];
    }
  }
  
  public static int[] inverter2( int[] list ) {
    int[] list1 = copy(list);
    inverter(list1);
    return list1;
  }
  
  public static int[] print( int[] list ) {
    for ( int i = 0; i < list.length; i++ ) {
      System.out.println("[" + list[i] + "] ");
    }
    System.out.println();
    return list;
  }
  
  public static void main(String[] args) { //creating main method
    int[] array0 = {1, 2, 3, 4, 5, 6, 7, 8};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    inverter(array0);
    print(array0);
    inverter2(array1);
    print(array1);
    int[] array3 = inverter2(array2);
    print(array3);
  }
  
}