//Natasha Baker
//lab07: Story Generator
//CSE2
//generates random sentences to form a story

import java.util.Random; //importing Random class
import java.util.Scanner; //importing scanner

public class StoryGenerator { //creating class
  public static void main(String[] args) { //creating main method
    Random randomGenerator = new Random(); //declaring randomGenerator
    Scanner myScanner = new Scanner(System.in); //declaring scanner
    
    String adjective = "0"; //initializing word types
    String subject = "0";
    String verb = "0";
    String object = "0";
    while (true) { //creating loop
      int randomAdj = randomGenerator.nextInt(10); //system selects a random number from 0-9 for each word
      int randomSub = randomGenerator.nextInt(10);
      int randomVer = randomGenerator.nextInt(10);
      int randomObj = randomGenerator.nextInt(10);
      adjective = Adj1( randomAdj ); //calling adj1 method
      subject = Sub1( randomSub ); //calling sub1 method
      verb = Ver1( randomVer ); //calling ver1 method
      object = Obj1( randomObj ); //calling obj1 method
      System.out.println("The " + adjective + " " + subject + " " + verb + " " + object + "."); //prints randomised words in a sentence
      System.out.println("WOULD YOU LIKE TO GENERATE THE REST OF THE STORY?"); //asking user if they want another sentence
      String initialSubject = subject;
      String ans = myScanner.next(); //accepting user input
      if ( ans.equals("yes")){ //if the user wants another sentence generated:
        int total = randomGenerator.nextInt(10);
        for ( int i = 0; i <= total; i++ ){ //repeats action sentence a random amount of times
          randomAdj = randomGenerator.nextInt(10); //system selects a random number from 0-9 for each word
          randomSub = randomGenerator.nextInt(2);
          randomVer = randomGenerator.nextInt(10);
          randomObj = randomGenerator.nextInt(10);
          adjective = Adj2( randomAdj ); //calling adj2 method
          subject = Sub2( randomSub, subject ); //calling sub2 method
          verb = Ver2( randomVer ); //calling ver2 method
          object = Obj2( randomObj ); //calling obj2 method
          System.out.println(subject + " was " + adjective + " that it " + verb + " " + object + ".");
          subject = initialSubject;
        }  
       System.out.println("The End.");
        
      }
      return;
      
    } //end of loop
  } //end of main method
  
  public static String Adj1( int a ) { //creating method for adj sentence
    String adjective = "0";
    if ( a == 0 ) { //generates an adjective
      adjective = "speedy";
    }
    else if ( a == 1 ){
      adjective = "lazy";
    }
    else if ( a == 2 ){
      adjective = "fat";
    }
    else if ( a == 3 ){
      adjective = "tiny";
    }
    else if ( a == 4 ){
      adjective = "giant";
    }
    else if ( a == 5 ){
      adjective = "grumpy";
    }
    else if ( a == 6 ){
      adjective = "sleepy";
    }
    else if ( a == 7 ){
      adjective = "hyperactive";
    }
    else if ( a == 8 ){
      adjective = "crazy";
    }
    else if ( a == 9 ){
      adjective = "mellow";
    }
    return adjective;
  }
    
  public static String Sub1( int s ) { //creating method for sub sentence
    String subject = "0";
    if ( s == 0 ){ //generating a subject
      subject = "dog";
    }
    else if ( s == 1 ){
      subject = "horse";
    }
    else if ( s == 2 ){
      subject = "cat";
    }
    else if ( s == 3 ){
      subject = "fox";
    }
    else if ( s == 4 ){
      subject = "mouse";
    }
    else if ( s == 5 ){
      subject = "squirrel";
    }
    else if ( s == 6 ){
      subject = "groundhog";
    }
    else if ( s == 7 ){
      subject = "bear";
    }
    else if ( s == 8 ){
      subject = "alpaca";
    }
    else if ( s == 9 ){
      subject = "llama";
    }
    return subject;
  }
    
  public static String Ver1( int v ) { //creating method for adj sentence
    String verb = "0";
    if ( v == 0 ){ //generating random verb
      verb = "walked by";
    }
    else if ( v == 1 ){
      verb = "ran by";
    }
    else if ( v == 2 ){
      verb = "crawled past";
    }
    else if ( v == 3 ){
      verb = "touched";
    }
    else if ( v == 4 ){
      verb = "smiled at";
    }
    else if ( v == 5 ){
      verb = "tiptoed by";
    }
    else if ( v == 6 ){
      verb = "grabbed";
    }
    else if ( v == 7 ){
      verb = "snatched";
    }
    else if ( v == 8 ){
      verb = "lay by";
    }
    else if ( v == 9 ){
      verb = "sat by";
    }
    return verb;
  }
  public static String Obj1( int o ) { //creating method for obj sentence
    String object = "0";
    if ( o == 0 ) { //generating random object
      object = "Taylor Gym";
    }
    else if ( o == 1 ) {
      object = "STEPS";
    }
    else if ( o == 2 ) {
      object = "the enemy";
    }
    else if ( o == 3 ) {
      object = "the dorm";
    }
    else if ( o == 4 ) {
      object = "Packard Lab";
    }
    else if ( o == 5 ) {
      object = "the UC";
    }
    else if ( o == 6 ) {
      object = "the tree";
    }
    else if ( o == 7 ){
      object = "my desk";
    }
    else if ( o == 8 ) {
      object = "a carrot";
    }
    else if ( o == 9 ) {
      object = "a pillow";
    }
     return object;
  } //end of object1 method
 
  public static String Sub2( int s, String b ) { //creating method for sub sentence
    String subject = "0";
    if ( s == 0 ){ //generating "it" or the previous subject
      subject = "It";
    }
    else if ( s == 1 ){
      subject = "This " + b;
    }
    return subject;
  }
    public static String Adj2( int a ) { //creating method for adj sentence
    String adjective = "0";
    if ( a == 0 ){
      adjective = "happy";
    }
    else if ( a == 1 ){
      adjective = "sad";
    }
    else if ( a == 2 ){
      adjective = "excited";
    }
    else if ( a == 3 ){
      adjective = "devastated";
    }
    else if ( a == 4 ){
      adjective = "overjoyed";
    }
    else if ( a == 5 ){
      adjective = "mortified";
    }
    else if ( a == 6 ){
      adjective = "embarrassed";
    }
    else if ( a == 7 ){
      adjective = "terrified";
    }
    else if ( a == 8 ){
      adjective = "didn't care";
    }
    else if ( a == 9 ){
      adjective = "ecstatic";
    }
    return adjective;
  }
    
  public static String Ver2( int v ) { //creating method for ver sentence    
    String verb = "0";  
    if ( v == 0 ){ //generating random verb
      verb = "sat on";
    }
    else if ( v == 1 ){
      verb = "stepped on";
    }
    else if ( v == 2 ){
      verb = "poked";
    }
    else if ( v == 3 ){
      verb = "saw";
    }
    else if ( v == 4 ){
      verb = "talked to";
    }
    else if ( v == 5 ){
      verb = "saved";
    }
    else if ( v == 6 ){
      verb = "hugged";
    }
    else if ( v == 7 ){
      verb = "ate";
    }
    else if ( v == 8 ){
      verb = "interacted with";
    }
    else if ( v == 9 ){
      verb = "picked up";
    }
    return verb;
  }
    
  public static String Obj2( int o ) { //creating method for action sentence
    String object = "0";
    if ( o == 0 ){ //generating random object
      object = "a piece of toast";
    }
    else if ( o == 1 ){
      object = "its mom";
    }
    else if ( o == 2 ){
      object = "some grass";
    }
    else if ( o == 3 ){
      object = "a rock";
    }
    else if ( o == 4 ){
      object = "a puddle";
    }
    else if ( o == 5 ){
      object = "a lint roller";
    }
    else if ( o == 6 ){
      object = "a pair of socks";
    }
    else if ( o == 7 ){
      object = "a burger";
    }
    else if ( o == 8 ){
      object = "an egg";
    }
    else if ( o == 9 ){
      object = "a pillow";
    }
    return object;
  } 
  
  
  
} //end of class