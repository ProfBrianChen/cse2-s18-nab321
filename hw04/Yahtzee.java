//Natasha Baker, CSE2, Homework 4, due Feb 20 2018
import java.util.Scanner; //importing scanner
public class Yahtzee { //creating class
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    System.out.println("Do you want to roll or choose your 5 numbers? (type 'roll' or 'choose')");
    String rollOrChoose = myScanner.next(); //accept user's choice
    
    int aces = 0; //initiaizing all amounts to zero
    int twos = 0;
    int threes = 0;
    int fours = 0;
    int fives = 0;
    int sixes = 0; 
    
    int threeKind = 0; //initallizing all lower section values to 0
    int fourKind = 0;
    int fullHouse = 0;
    int smallStraight = 0;
    int largeStraight = 0;
    int yahtzee = 0;
    int chance = 0;
    
    switch ( rollOrChoose ) {
      case "roll": //the user chooses to roll the dice, the following will happen:
       int die1 = (int)(Math.random()*6)+1; //randomly rolls dice
       int die2 = (int)(Math.random()*6)+1;
       int die3 = (int)(Math.random()*6)+1;
       int die4 = (int)(Math.random()*6)+1;
       int die5 = (int)(Math.random()*6)+1;
       System.out.println("You rolled "+die1+die2+die3+die4+die5);
        
       switch ( die1 ) {
         case 1:
           ++aces;
           break;
         case 2:
           ++twos;
           break;
         case 3:
           ++threes;
           break;
         case 4:
           ++fours;
           break;
         case 5:
           ++fives;
           break;
         case 6:
           ++sixes;
           break;
       }  
       switch ( die2 ) {
         case 1:
           ++aces;
           break;
         case 2:
           ++twos;
           break;
         case 3:
           ++threes;
           break;
         case 4:
           ++fours;
           break;
         case 5:
           ++fives;
           break;
         case 6:
           ++sixes;
           break;
       }  
       switch ( die3 ) {
         case 1:
           ++aces;
           break;
         case 2:
           ++twos;
           break;
         case 3:
           ++threes;
           break;
         case 4:
           ++fours;
           break;
         case 5:
           ++fives;
           break;
         case 6:
           ++sixes;
           break;
       }  
        switch ( die4 ) {
         case 1:
           ++aces;
           break;
         case 2:
           ++twos;
           break;
         case 3:
           ++threes;
           break;
         case 4:
           ++fours;
           break;
         case 5:
           ++fives;
           break;
         case 6:
           ++sixes;
           break;
       }  
       switch ( die5 ) {
         case 1:
           ++aces;
           break;
         case 2:
           ++twos;
           break;
         case 3:
           ++threes;
           break;
         case 4:
           ++fours;
           break;
         case 5:
           ++fives;
           break;
         case 6:
           ++sixes;
           break;
       }  
        
      break;
      //end of code for case: roll 
        
        
      case "choose": //the user choses to choose their numbers, the following will happen:
       System.out.println("Type 5 digits including only numbers 1-6 (one digit per line)");
       int digit1 = myScanner.nextInt(); //accept user's number
       if ( digit1 <= 0 || digit1 >= 7 ) { //does not accept user's number if not 1-6
         System.out.println("ERROR. PLASE TYPE ONE DIGIT PER LINE FROM 1-6");
       break;
       }
       int digit2 = myScanner.nextInt(); //accept user's number 
       if ( digit2 <= 0 || digit2 >= 7 ) { //does not accept user's number if not 1-6
         System.out.println("ERROR. PLASE TYPE ONE DIGIT PER LINE FROM 1-6");
       break;
       }
       int digit3 = myScanner.nextInt(); //accept user's number
       if ( digit3 <= 0 || digit3 >= 7 ) { //does not accept user's number if not 1-6
         System.out.println("ERROR. PLASE TYPE ONE DIGIT PER LINE FROM 1-6");
       break;
       }
       int digit4 = myScanner.nextInt(); //accept user's number
       if ( digit4 <= 0 || digit4 >= 7 ) { //does not accept user's number if not 1-6
         System.out.println("ERROR. PLASE TYPE ONE DIGIT PER LINE FROM 1-6");
       break;
       }
       int digit5 = myScanner.nextInt(); //accept user's number
       if ( digit5 <= 0 || digit5 >= 7 ) { //does not accept user's number if not 1-6
         System.out.println("ERROR. PLASE TYPE ONE DIGIT PER LINE FROM 1-6"); 
       break;
       }
       System.out.println("You chose "+digit1+digit2+digit3+digit4+digit5); //prints digits user chose
       
       switch ( digit1 ) {
         case 1:
           ++aces;
           break;
         case 2:
           ++twos;
           break;
         case 3:
           ++threes;
           break;
         case 4:
           ++fours;
           break;
         case 5:
           ++fives;
           break;
         case 6:
           ++sixes;
           break;
       } 
       switch ( digit2 ) {
         case 1:
           ++aces;
           break;
         case 2:
           ++twos;
           break;
         case 3:
           ++threes;
           break;
         case 4:
           ++fours;
           break;
         case 5:
           ++fives;
           break;
         case 6:
           ++sixes;
           break;
       } 
       switch ( digit3 ) {
         case 1:
           ++aces;
           break;
         case 2:
           ++twos;
           break;
         case 3:
           ++threes;
           break;
         case 4:
           ++fours;
           break;
         case 5:
           ++fives;
           break;
         case 6:
           ++sixes;
           break;
       } 
       switch ( digit4 ) {
         case 1:
           ++aces;
           break;
         case 2:
           ++twos;
           break;
         case 3:
           ++threes;
           break;
         case 4:
           ++fours;
           break;
         case 5:
           ++fives;
           break;
         case 6:
           ++sixes;
           break;
       } 
       switch ( digit5 ) {
         case 1:
           ++aces;
           break;
         case 2:
           ++twos;
           break;
         case 3:
           ++threes;
           break;
         case 4:
           ++fours;
           break;
         case 5:
           ++fives;
           break;
         case 6:
           ++sixes;
           break;
       } 
       
       
        
      break; 
      //end of code for case: choose 
        
      default:
       System.out.println("Error. Please type 'roll' or 'choose'."); //prints error if user doesn't write roll or choose
       break;     
    } 
    
    //start of calculating lower section
    switch ( aces ) {
        case 5:
          yahtzee += 50;
          break;
        case 4:
          fourKind += 4;
          break;
        case 3:
          threeKind += 3;
          break;
      }
    switch ( twos ) {
        case 5:
          yahtzee += 50;
          break;
        case 4:
          fourKind += 8;
          break;
        case 3:
          threeKind += 6;
          break;
      }
    switch ( threes ) {
        case 5:
          yahtzee += 50;
          break;
        case 4:
          fourKind += 12;
          break;
        case 3:
          threeKind += 9;
          break;
      }
    switch ( fours ) {
        case 5:
          yahtzee += 50;
          break;
        case 4:
          fourKind += 16;
          break;
        case 3:
          threeKind += 12;
          break;
      }    
    switch ( fives ) {
        case 5:
          yahtzee += 50;
          break;
        case 4:
          fourKind += 20;
          break;
        case 3:
          threeKind += 15;
          break;
      }
    switch ( sixes ) {
        case 5:
          yahtzee += 50;
          break;
        case 4:
          fourKind += 24;
          break;
        case 3:
          threeKind += 18;
          break;
      }
    System.out.println("Aces: "+aces); //prints total of each possible number to roll
    System.out.println("Twos: "+twos);
    System.out.println("Threes: "+threes);
    System.out.println("Fours: "+fours);
    System.out.println("Fives: "+fives);
    System.out.println("Sixes: "+sixes);
    
    int upper = aces*1 + twos*2 + threes*3 + fours*4 + fives*5 + sixes*6; //upper section total
    System.out.println("Upper Section Total: "+upper);
    
    //calculating large and small straight
    if ( aces==1 && twos==1 && threes==1 && fours==1 && fives==1 ) {
      largeStraight += 40;
    }
    else if ( twos==1 && threes==1 && fours==1 && fives==1 && sixes==1 ) {
      largeStraight += 40;
    }
    else if ( aces>=1 && twos>=1 && threes>=1 && fours>=1 ) {
      smallStraight += 30;
    }
    else if ( twos>=1 && threes>=1 && fours>=1 && fives>=1 ) {
      smallStraight += 30;
    }
    else if ( threes>=1 && fours>=1 && fives>=1 && sixes>=1 ) {
      smallStraight += 30;
    }
    
    //calculating full house
    if ( aces>=3 && ( twos>=2 || threes>=2 || fours>=2 || fives>=2 || sixes>=2 ) ) {
      fullHouse += 25;
    }
    else if ( twos>=3 && ( aces>=2 || threes>=2 || fours>=2 || fives>=2 || sixes>=2 ) ) {
      fullHouse += 25;
    }
    else if ( threes>=3 && ( aces>=2 || twos>=2 || fours>=2 || fives>=2 || sixes>=2 ) ) {
      fullHouse += 25;
    }
    else if ( fours>=3 && ( aces>=2 || threes>=2 || twos>=2 || fives>=2 || sixes>=2 ) ) {
      fullHouse += 25;
    }
    else if ( fives>=3 && ( aces>=2 || threes>=2 || fours>=2 || twos>=2 || sixes>=2 ) ) {
      fullHouse += 25;
    }
    else if ( sixes>=3 && ( aces>=2 || threes>=2 || fours>=2 || fives>=2 || twos>=2 ) ) {
      fullHouse += 25;
    }
  
    System.out.println("Three of a Kind: "+threeKind); //prints out the totals of all the combinations
    System.out.println("Four of a Kind: "+fourKind);
    System.out.println("Yahtzee: "+yahtzee);
    System.out.println("Small Straight: "+smallStraight);
    System.out.println("Large Straight: "+largeStraight);
    System.out.println("Full House: "+fullHouse);
    System.out.println("Chance: "+upper);
    
    int lower = threeKind+fourKind+yahtzee+fullHouse+smallStraight+largeStraight+upper;
    System.out.println("Lower section total: "+lower);
    int total = upper+lower+threeKind+fourKind+yahtzee+smallStraight+largeStraight+upper;
    System.out.println("Total Score: "+total);
    
  }
}