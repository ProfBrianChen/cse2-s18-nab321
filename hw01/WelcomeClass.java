// Natasha Baker, Homework 1, CSE 2, due Feb 6 2018
public class WelcomeClass { //creating class
  public static void main(String [] args) { //creating main method
    System.out.println("  -----------"); //print dashes
    System.out.println("  | WELCOME |"); //prints "Welcome" to console
    System.out.println("  -----------"); //print dashes
    System.out.println("  ^  ^  ^  ^  ^  ^  "); //print spikes
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ "); //print slashes
    System.out.println("<-N--A--B--3--2--1->"); //print network ID
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / "); //print slashes
    System.out.println("  v  v  v  v  v  v  "); //print v's
  }
}