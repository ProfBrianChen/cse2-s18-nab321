//splitting the check in different ways
import java.util.Scanner;
public class Check{ //creating class
    public static void main(String[] args) {
      Scanner myScanner = new Scanner( System.in ); //constructing scanner
      System.out.println("Enter the original cost of the check in the form xx.xx: "); //promts user to input check amount
      double checkCost = myScanner.nextDouble(); //accepts user input
      System.out.println("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
      double tipPercent = myScanner.nextDouble();
      tipPercent = 100; //convert the percentage into a decimal value
      System.out.println("Enter the number of people who went out to dinner: ");
      int numPeople = myScanner.nextInt();
      double totalCost;
      double costPerPerson;
      int dollars, dimes, pennies;
      totalCost = checkCost * (1 + tipPercent);
      costPerPerson = totalCost / numPeople; //calculates cost per person
      dollars=(int)costPerPerson; //get the whole amount, dropping decimal fraction
      dimes=(int)(costPerPerson * 10) % 10; //get dimes amount
      pennies=(int)(costPerPerson * 100) % 10;
      System.out.println("Each person in the group owes $" +dollars +"."+dimes+pennies);    
}  //end of main method   
  	} //end of class
