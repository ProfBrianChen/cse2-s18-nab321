//Natasha Baker
//CSE2 hw08 Program 1: one dimensional arrays and searching
//due April 10 2018

import java.util.Scanner; //importing scanner
import java.util.Random; //importing randomizer

public class CSE2Linear { //creating class
  public static void main(String[] args) { //creating main method
    Scanner myScanner = new Scanner(System.in); //declaring scanner
    int[] grade; //declaration of array for student grades
    int numStudents = 15; //total number of student grades
    grade = new int[numStudents]; //setting array length
    System.out.println("PLEASE ENTER 15 INTEGERS FOR STUDENTS' FINAL GRADES IN CSE2 IN INCREASING ORDER:"); //promts user to input student grades
    
    for ( int i = 0; i < numStudents; i++ ) { //creating loop for user to input student grades
      while ( !myScanner.hasNextInt() ) { //makes sure user inputs an integer
        String junk = myScanner.next(); //sets input to junk
        System.out.println("PLEASE ENTER AN INTEGER.");
      }
      while ( myScanner.nextInt() < 0 ) { //makes sure user inputs an integer from 0 to 100
        int junk = myScanner.nextInt(); //sets input to junk
        System.out.println("PLEASE ENTER AN INTEGER FROM 0 TO 100.");
      }
      while ( myScanner.nextInt() > 100 ) { //makes sure user inputs an integer from 0 to 100
        int junk = myScanner.nextInt(); //sets input to junk
        System.out.println("PLEASE ENTER AN INTEGER FROM 0 TO 100.");
      }
      grade[i] = myScanner.nextInt(); //accepts user input for grade
      if ( grade[i+1] > grade[i] ) {
        System.out.println("PLEASE INPUT A GRADE GREATER THAN OR EQUAL TO THE LAST GRADE INPUTTED.");
        i--;
      }
    } 
    
    for ( int j = 0; j < numStudents; j++ ) { //loop to print all of the grades
      System.out.print( grade[j] + " "); //prints out members of array
    }
    System.out.println(); //prints the following on a new line
    
    
    System.out.print("ENTER A GRADE TO SEARCH FOR: "); //binary search
    int searchB = myScanner.nextInt();
    boolean Bfound;
    binary(grade, searchB);
    if ( Bfound = true ) {
      System.out.println( searchB + " WAS FOUND IN THE LIST.");
    }
    else if ( Bfound = false ) {
      System.out.println( searchB + " WAS NOT FOUND IN THE LIST.");
    }
    
    
    scramble(grade); //calling shuffling method to scramble the members of the array
    for ( int i = 0; i < numStudents; i++ ) { //loop to print out shuffled array
      System.out.print(grade[i] + " "); 
    }
    System.out.println(); //prints the following on a new line
    
    
    System.out.print("ENTER A GRADE TO SEARCH FOR: "); //prompts user to input an integer to search
    int searchL = myScanner.nextInt(); //creates new variable searchL to accept user input
    boolean found;
    linear(grade, searchL); //calling linear search method
    if ( found = true ) { //if the input was found:
      System.out.println( searchL + " WAS FOUND IN THE LIST." );
    }
    else if ( found = false ) { //if the input was not found
      System.out.println( searchL + " WAS NOT FOUND IN THE LIST." );
    }
     
  } //end of main method
  
  public static boolean linear (int[] grade, int search) { //method to perform linear search
    boolean found;
    int numStudents = 15;
    for ( int i = 0; i < numStudents; i++ ) {
      if ( grade[i] == search ) { //if the searched value equals one of the members
        found = true; //boolean found is set to true
        break;
      }
      else {
        found = false; //if the searched value is not found, boolean found is set to false
      }
    }
    return found; //returns whether found is true or false
  }
  
  public static boolean binary (int[] list, int search) { //method to perform binary search
    boolean Bfound = false;
    int begin = 0;
    int end = list.length;
    while ( begin < end ) {
      int mid = ((begin-end)/2) + begin;
      if ( mid == search ) {
        Bfound = true;
        break;
      }
      if ( search > list[mid] ) {
        begin = mid + 1;
      }
      if ( search < list[mid]) {
        end = mid - 1;
      }
    }
    return Bfound; //returns whether Bfound is true or false
  }
  
  public static void scramble ( int[] grade ) { //method to shuffle the members of the grade array
    for ( int i = 0; i < grade.length; i++ ) {
      int first = (int) ( grade.length*Math.random() ); //picks a random member to swap with
      int second = grade[first]; 
      grade[first] = grade[i];
      grade[i] = second;
    }
  }
  
} //end of class 