//Natasha Baker
//CSE2 hw08 Program 2: writing methods for given code
//due April 10 2018

import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
  public static void main(String [] arg){
	  Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
	  String answer="";
	  do{
  	  System.out.print("Random input 10 ints [0-9]");
    	num = randomInput();
  	  String out = "The original array is:";
  	  out += listArray(num);
  	  System.out.println(out);
 
  	  System.out.print("Enter the index ");
  	  index = scan.nextInt();
  	  newArray1 = delete(num,index);
  	  String out1="The output array is ";
  	  out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
    	System.out.println(out1);
 
      System.out.print("Enter the target value ");
    	target = scan.nextInt();
    	newArray2 = remove(num,target);
    	String out2="The output array is ";
    	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
    	System.out.println(out2);
  	 
    	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
    	answer=scan.next();
  	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	  String out="{";
	  for(int j=0;j<num.length;j++){
  	  if(j>0){
      	out+=", ";
    	}
  	  out+=num[j];
	  }
	  out+="} ";
	  return out;
  }
  public static int[] randomInput() { //creating method called randomInput
		Random randomizer=new Random();
		int total = 10; //array length = 10
    int[] random = new int[total]; //creating array with length total
		for ( int i = 0; i < total; i++ ) { //creating loop 
			random[i] = randomizer.nextInt(10); //setting each array member to a random integer
		}
		return random; //returns the array
  }
	
  public static int[] delete( int[] list, int pos ) {
    int[] list2 = new int[list.length-1];
		for ( int i = 0; i < list.length -1; i++ ){
			list2[i] = list[i];
			if ( list[i] == pos ) {
				for ( int j = 0; j < listlength-1; j++ ) {
					list2[i] = list2[i+1];
				}
				break;
			}
		}
		return list2;
  }
	
  public static int[] remove( int[] list, int target ) {
    int[] list3 = new int[list.length];
		int iterations = 0;
		for ( int i = 0; i < list.length-iterations; i++ ) {
			list3[i] = list[i];
			if ( list[i] == target ) {
				list3[i] = list3[i+1];
				iterations+=1;
			}
		}
		int[] list4 = new int[list.length-iterations];
		for ( int j = 0; j < list.length-iterations; j++ ) {
			list4[i] = list[i];
			if ( list[i] == target ) {
				list4[i] = list4[i+1];
			}
		}
		return list4;
  }
  
}
 

