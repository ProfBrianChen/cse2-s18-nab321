//creating twists from user input
import java.util.Scanner; //importing scanner
public class TwistGenerator { //creating main method
  public static void main(String[] args) {
    Scanner myScanner = new Scanner ( System.in ); //constructing scanner
    
    System.out.println("Please input a twist integer");
    while ( !myScanner.hasNextInt() ) { //consructing loop
      String junkword = myScanner.next();
      System.out.println("ERROR. PLEASE INPUT AN INTEGER.");
    }
    int capacity = myScanner.nextInt(); //accepts user input
    while ( capacity <= 0 ) { //makes sure integer is not negative
      System.out.println("ERROR. PLEASE ENTER A POSITIVE INTEGER.");
      while ( !myScanner.hasNextInt() ) { //making sure user inputs an integer
       String junkword = myScanner.next();
       System.out.println("ERROR. PLEASE ENTER AN INTEGER."); //prints error
      }
      capacity = myScanner.nextInt(); 
    }
    
    final int fcap = capacity; //setting fcap to capacity so that capacity doesn't change
    
      int integer = 1; //initializing integer to 1
      while ( integer <= fcap ) { //first line
        if ( integer%3 == 1 ) { //what to print when the remainder is 1 
          System.out.print("\\");
          integer++; //adds one to integer
        }
        else if ( integer%3 == 2 ) {
          System.out.print(" ");
          integer++;
        }
        else if ( integer%3 == 0 ) {
          System.out.print("/");
          integer++;
        }
      }
      System.out.println(); //adds a line
      integer = 1; //resetting integer to 1
      while ( integer <= fcap ) { //second line
        if (integer%3 == 1 ) {
          System.out.print(" ");
          integer++;
        }
        else if ( integer%3 == 2 ) {
          System.out.print("X");
          integer++;
        }
        else if ( integer%3 == 0 ) {
          System.out.print(" ");
          integer++;
        }
      }
      System.out.println(); //adds a line
      integer = 1; //resetting integer to 1
      while ( integer <= fcap ) { //third line
        if ( integer%3 == 1 ) {
          System.out.print("/");
          integer++;
        }
        else if ( integer%3 == 2 ) {
          System.out.print(" ");
          integer++;
        }
        else if ( integer%3 == 0 ) {
          System.out.print("\\");
          integer++;
        }
      }
      System.out.println();
    
  }
}