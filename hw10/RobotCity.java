//Natasha Baker
//CSE 2: hw10
//due April 24 2018
//working with multidimensional arrays

import java.util.Random; //importing Random class

public class RobotCity { //creating class
  
  public static int[][] buildCity (int[][] cityArray, int width, int height) { //creating build city method
    Random randomGenerator = new Random(); //initialising randomGenerator
    for ( int i = 0; i < height; i++ ) { //creating outer for loop for height
      for ( int j = 0; j < width; j++ ) { //creating inner for loop for width
        cityArray[i][j] = randomGenerator.nextInt(899)+100;
      }
    }
    return cityArray; //returns the cityArray with designated populations per block
  }
  
  public static void printCity (int[][] cityArray, int width, int height) { //creating method to print the city
    for ( int i = 0; i < height; i++ ) { //outer loop for height of the city
      for ( int j = 0; j < width; j++ ) { //inner loop for width of the city
        System.out.printf("[" + cityArray[i][j] + "]"); //prints the population of each block within brackets
      }
      System.out.println(); //prints next row on new line
    }
  }
  
  public static void invade (int[][] cityArray, int k, int width, int height) { //method to invade the city
    Random randomGenerator = new Random(); //declaring randomGenerator
    for ( int r = 0; r < k; r++ ) { //one block per robot
      int randomWidth = randomGenerator.nextInt(width); //generates a random width coordinate
      int randomHeight = randomGenerator.nextInt(height); //generates a random height coordinate
      if ( cityArray[randomHeight][randomWidth]+cityArray[randomHeight][randomWidth] < 0 ) { //if population is negative
          continue; //starts loop over so that the population is not brought back to life
        }
        else if ( cityArray[randomHeight][randomWidth]+cityArray[randomHeight][randomWidth] > 0 ) { //if population is positive
          cityArray[randomHeight][randomWidth] = (-1)*cityArray[randomHeight][randomWidth]; //sets the random coordinates to the negative version of it
        } 
    }
    for ( int i = 0; i < height; i++ ) { //outer loop for height of the city
      for ( int j = 0; j< width; j++ ) { //inner loop for width of the city
        if ( cityArray[i][j]+cityArray[i][j] < 0 ) { //if the value is negative
          System.out.printf("[" + cityArray[i][j] + "]"); //prints the population for each block
        }
        else if ( cityArray[i][j]+cityArray[i][j] > 0 ) { //if the value is positive
          System.out.printf("[0" + cityArray[i][j] + "]"); //prints the value with a zero in front so that the size is even
        } 
      }  
      System.out.println(); //prints next row on a new line
    }
    System.out.println(); //print an empty line
  }
  
  public static void update (int[][] cityArray, int width, int height) { //method to print the updated array
    for ( int i = 0; i < height; i++ ) { //outer loop for height
      for ( int j = 0; j < width; j++ ) { //inner loop for width
        if ( cityArray[i][j] + cityArray[i][j] < 0 ) { //if the value is negative
          if ( j < width - 1 ) { //if the array width block number is the second to last block and fewer
            if ( cityArray[i][j+1] < 0 ) { //if the block following it is already negative
              continue; //nothing happens
            }
            else if ( cityArray[i][j+1] > 0 ) { //if the block following it is positive
              cityArray[i][j+1] = (-1)*cityArray[i][j+1]; //set the block after it to negative
              continue;
            } 
          }
          else if ( j == width -1 ) { //if the block is the last block in the width
            continue; //nothing happens
          }  
        }
        else if ( cityArray[i][j] + cityArray[i][j] > 0 ) { //if the value is positive
          continue; //nothing happens
        }
      }
    } 
    for ( int i = 0; i < height; i++ ) { //outer loop for height of the city
      for ( int j = 0; j< width; j++ ) { //inner loop for width of the city
        if ( cityArray[i][j]+cityArray[i][j] < 0 ) { //if the value is negative
          System.out.printf("[" + cityArray[i][j] + "]"); //prints the population for each block
        }
        else if ( cityArray[i][j]+cityArray[i][j] > 0 ) { //if the value is positive
          System.out.printf("[0" + cityArray[i][j] + "]"); //prints the value with a zero in front so that the size is even
        } 
      }  
      System.out.println(); //prints next row on a new line
    }
  }
  
  public static void main(String[] args) { //creating main method
    Random randomGenerator = new Random(); //declaring randomGenerator
    int width = randomGenerator.nextInt(5)+10; //setting width equal to a random int from 10-15
    int height = randomGenerator.nextInt(5)+10; //setting height equal to a random int from 10-15
    int[][]cityArray = new int[height][width]; //creating a new array
    buildCity(cityArray, width, height); //calling buildCity method
    printCity(cityArray, width, height); //calling printCity method
    System.out.println(); //prints a new line   
    int k = randomGenerator.nextInt(width*height); //sets number of robots invading to a random number less than or equal to number of blocks
    invade(cityArray, k, width, height); //calling invade method
    for ( int i = 0; i < 5; i++ ) { //runs the program 5 times   
      update(cityArray, width, height); //calls the update method to print the city post invasion
      System.out.println(); //adds an empty line so each update is clear
    }
  }
}