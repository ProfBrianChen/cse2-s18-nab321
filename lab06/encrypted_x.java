//encrypt an "x" within *'s
import java.util.Scanner; //importing scanner
public class encrypted_x { //creating main method
  public static void main(String[] args) {
    Scanner myScanner = new Scanner ( System.in );
    System.out.println("PLEASE TYPE AN INTEGER FROM 0-100"); //promts user to input an integer
    while ( !myScanner.hasNextInt() ) { //makes sure the number is an integer
      String junkword = myScanner.next();
      System.out.println("ERROR. PLEASE INPUT AN INTEGER.");
    }
    int input = myScanner.nextInt(); //accepts user input
    while ( input < 0 || input > 100 ) { //makes sure integer is between 0 and 100
      System.out.println("ERROR. PLEASE ENTER A POSITIVE INTEGER.");
      while ( !myScanner.hasNextInt() ) { //making sure user inputs an integer
       String junkword = myScanner.next();
       System.out.println("ERROR. PLEASE ENTER AN INTEGER."); //prints error
      }
      input = myScanner.nextInt(); 
    }
    int diagonal = input-1;
    final int finput = input; //setting finput to input so that capacity doesn't change
    
    
    for ( int i = 0; i < finput; i++ ) { //creating outer loop
      for ( int j = 0; j < finput; j++ ) { //creating inner loop
        if ( j == i ) { //creating spaces in first diagonal
          System.out.print(" ");
        }
        else if ( j == diagonal ) { //creating spaces in second diagonal
          System.out.print(" ");
          diagonal--;
        }
        else { //* will be printed if not in either diagonal
          System.out.print("*");
        }
      }
      System.out.println(); //prints on next line
    } 
    
  }
}