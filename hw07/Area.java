//Natasha Baker
//CSE2
//due March 27 2018
//calculating the area of a rectangle, triangle, or circle

import java.util.Scanner;
public class Area { //creating main method
  public double cirArea ( radius ) //creating method to calculate area of circle
    double area = Math.PI*Math.pow(radius, 2); //formula for the area of a circle
    System.out.println("AREA OF THE CIRCLE: " + area); //prints the area
  }
  public double recArea ( height, width ) { //creating method to calculate area of rectangle
    double area = height*width; //formula for the area of a rectangle
    System.out.println("AREA OF RECTANGLE: " + area); //prints the area
  }
  public double triArea ( height, width ) { //creating method for area of triangle
    double area = (height*width)/2; //formula for the area of a triangle
    System.out.println("AREA OF TRIANGLE: " + area); //prints the area
  }
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); //creating scanner
		System.out.println("PLEASE INPUT THE NAME OF THE SHAPE (RECTANGLE, TRIANGLE, OR CIRCLE) FOR WHICH YOU WOULD LIKE TO CALCULATE THE AREA.");
		String shape = myScanner.next(); //creating string for shape name
		 while ( shape != "circle" || "rectangle" || "triangle" ) { //makes sure the shape is valid
     		 String junkword = myScanner.next(); //accepts user input as junk
  	            System.out.println("ERROR INVALID SHAPE. PLEASE INPUT ‘circle’, ‘rectangle’ or ‘triangle’."); 
    }
		if ( shape = circle ) { //if the user chooses circle the following will happen
			System.out.println("PLEASE INPUT THE RADIUS OF THE CIRCLE.");
			double radius = myScanner.nextDouble(); //accepts user input
			cirArea(radius); //calls the circle area method
		}
		else if ( shape = rectangle ) { //if the user chooses rectangle:
			System.out.println("PLEASE INPUT THE WIDTH OF THE RECTANGLE.");
			double width = myScanner.nextDouble(); //accepts user input
			System.out.println("PLEASE INPUT THE HEIGHT OF THE RECTANGLE.");
			double height = myScanner.nextDouble(); //accepts user input
			recArea(width, height); //calls the rectangle area method
		}
		else if ( shape = triangle ) { //if the user chooses triangle:
			System.out.println("PLEASE INPUT THE HEIGHT OF THE TRIANGLE.");
			double height = myScanner.nextDouble(); //accepts user input
			System.out.println("PLEASE INPUT THE WIDTH OF THE TRIANGLE.");
			double width = myScanner.nextDouble(); //accepts user input
			triArea(height, width); //calls triangle area method
		}
		
	}
}