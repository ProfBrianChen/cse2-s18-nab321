//Natasha Baker
//Hw06, CSE2
//due March 6 2018, 11pm
//asking user for information of they classes

import java.util.Scanner; //importing scanner
public class Hw05 { //creating main method
  public static void main(String [] args) {
    Scanner myScanner = new Scanner (System.in); //creating scanner
    
    //course number
    System.out.println("WHAT IS THE COURSE NUMBER?"); //asks user to input course number
    while ( !myScanner.hasNextInt() ) { //creating loop
      String junkword = myScanner.next();
      System.out.println("ERROR. PLEASE ENTER AN INTEGER."); //prints error
    }
    int number = myScanner.nextInt(); //accepts user input
    while ( number <= 0 ) { //makes sure integer is not negative
      System.out.println("ERROR. PLEASE ENTER A POSITIVE INTEGER.");
      while ( !myScanner.hasNextInt() ) { //making sure user inputs an integer
      String junkword = myScanner.next();
      System.out.println("ERROR. PLEASE ENTER AN INTEGER."); //prints error
      }
      number = myScanner.nextInt();
    }
    
    //department name
    System.out.println("WHAT IS THE DEPARTMENT NAME?");
    while ( !myScanner.hasNext() ) { //creating loop
      String junkword = myScanner.next(); //if user inputs incorrect variable
      System.out.println("ERROR. PLEASE ENTER THE DEPARTMENT NAME.");
    }
    String department = myScanner.next();
    
    //meetings per week
    System.out.println("HOW MANY TIMES DO YOU MEET A WEEK?");
    while ( !myScanner.hasNextInt() ) {
      String junkword = myScanner.next();
      System.out.println("ERROR. PLEASE ENTER AN INTEGER.");
    }
    int meet = myScanner.nextInt();
    while ( meet <= 0 ) { //makes sure integer is not negative
      System.out.println("ERROR. PLEASE ENTER A POSITIVE INTEGER.");
      while ( !myScanner.hasNextInt() ) { //making sure user inputs an integer
      String junkword = myScanner.next();
      System.out.println("ERROR. PLEASE ENTER AN INTEGER."); //prints error
      }
      meet = myScanner.nextInt();
    }
    
    //class starting time
    System.out.println("WHAT TIME DOES THE CLASS START?");
    while ( !myScanner.hasNextInt() ) {
      String junkword = myScanner.next();
      System.out.println("ERROR. PLEASE ENTER AN INTEGER.");
    }
    int start = myScanner.nextInt();
    while ( start <= 0 ) { //makes sure integer is not negative
      System.out.println("ERROR. PLEASE ENTER A POSITIVE INTEGER.");
      while ( !myScanner.hasNextInt() ) { //making sure user inputs an integer
      String junkword = myScanner.next();
      System.out.println("ERROR. PLEASE ENTER AN INTEGER."); //prints error
      }
      start = myScanner.nextInt();
    }
    
    //instructor name
    System.out.println("WHAT IS THE NAME OF THE INSTRUCTOR?");
    while ( !myScanner.hasNext() ) {
      String junkword = myScanner.next();
      System.out.println("ERROR. PLEASE ENTER YOUR INSTRUCTOR'S NAME.");
    }
    String instructor = myScanner.next();
    
    //class size
    System.out.println("HOW MANY STUDENTS ARE IN THE CLASS?");
    while ( !myScanner.hasNextInt() ) {
      String junkword = myScanner.next();
      System.out.println("ERROR. PLEASE INPUT AN INTEGER.");
    }
    int size = myScanner.nextInt();
    while ( size <= 0 ) { //makes sure integer is not negative
      System.out.println("ERROR. PLEASE ENTER A POSITIVE INTEGER.");
      while ( !myScanner.hasNextInt() ) { //making sure user inputs an integer
      String junkword = myScanner.next();
      System.out.println("ERROR. PLEASE ENTER AN INTEGER."); //prints error
      }
      size = myScanner.nextInt();
    }
  }
}
