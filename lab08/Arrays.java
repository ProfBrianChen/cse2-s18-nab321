//Natasha Baker
//CSE2
//lab08: One Dimensional Arrays
//April 6 2018

import java.util.Scanner; //importing scanner
import java.util.Random; //importing Random

public class Arrays { //creating class
  public static void main(String[] args) { //creating main method
    Scanner myScanner = new Scanner(System.in); //declaring scanner
    Random randomGenerator = new Random(); //declaring random generator
    int numStudents = randomGenerator.nextInt(6) + 5; //initialising number of students
    String[] students; //declaration of array for student names
    students = new String[numStudents]; //setting array length
    System.out.println("ENTER " + numStudents + " STUDENT NAMES:"); //prompts user for student names
    for ( int i = 0; i < numStudents; i++ ) { //creating loop for user to input student names
      students[i] = myScanner.next(); //accepts user input
    }
    
    int[] midterm; //declaration of array for student midterm grades
    midterm = new int[numStudents]; //setting array length
    System.out.println("HERE ARE THE MIDTERM GRADES OF THE " + numStudents + " STUDENTS ABOVE:");
    for ( int j = 0; j < numStudents; j++ ) { //creating loop for user to input student grades
      System.out.print(students[j] + ": "); //prints student name
      midterm[j] = randomGenerator.nextInt(101); //randomizes midterm grades
      System.out.println( midterm[j] ); //prints randomized grade
    }
    
  } //end of main method
} //end of class Arrays