//Natasha Baker, Homework 2, CSE2, due Feb 13 2018
import java.util.Scanner;
public class Pyramid{ //creating class
    public static void main(String[] args) {
      Scanner myScanner = new Scanner( System.in ); //constructing scanner
      System.out.println("The square side of the pyramid is (input length): "); //promts user to input length
      double length = myScanner.nextDouble(); //accepts user input
      System.out.println("The height of the pyramid is (input height): "); //prompts user to input height
      double height = myScanner.nextDouble(); //accepts user input
      double volume;
      volume = (length*length*height)/3; //calculates volume of the pyramid
      System.out.println("The volume inside the pyramid is: "+volume); //prints the volume of the pyramid
    }
}
      