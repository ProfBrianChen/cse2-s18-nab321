//Natasha Baker, Homework 2, CSE2, due Feb 13 2018
import java.util.Scanner;
public class Convert{ //creating class
    public static void main(String[] args) {
      Scanner myScanner = new Scanner( System.in ); //constructing scanner
      System.out.println("Enter the affected area in acres: "); //promts user to input area in acres
      double areaAcres = myScanner.nextDouble(); //accepts user input
      System.out.println("Enter the rainfall in affected area: "); //promts user to input rainfall
      double rainfall = myScanner.nextDouble(); //accepts user input
      double cubicMiles;
      cubicMiles = areaAcres*rainfall*0.000000024660669; //converts arce inches to cubic miles
      System.out.println(cubicMiles+" cubic miles"); //prints answer in cubic miles
    }
}